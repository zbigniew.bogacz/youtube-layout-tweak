# YouTube Layout Tweak

Browser extension for improving YouTube homepage layout to be more desktop friendly.

Allows to set number of videos in a row on YouTube Homepage between 4-10 videos in a row (setting on addon options page).

### Links

- [Firefox](https://addons.mozilla.org/pl/firefox/addon/youtube-layout-tweak/)

### Screenshots

##### Homepage with 6 videos in a row:

![home](screenshots/home6.jpg)

##### Homepage with 10 videos in a row:

![home](screenshots/home10.jpg)
