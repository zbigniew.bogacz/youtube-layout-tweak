/*
Modifu function for calculating number of videos in row to always return maximum number of videos.
Then refresh layout by firing resize event.
*/

var maxItems = 6;
var calcElements = function (a, b) {
    if (a == 320) {
        return maxItems;
    }
    return b;
}

function fixLayout() {
    var cw = window.wrappedJSObject || window;
    if (!cw.document.querySelector('#primary > ytd-rich-grid-renderer')) {
        return;
    }
    let browse = window.chrome || browser;
    browse.storage.local.get(['items'], (res) => {
        maxItems = res.items || maxItems;
        if (typeof cloneInto === 'function') {
            cw.document.querySelector('#primary > ytd-rich-grid-renderer').calcElementsPerRow = cloneInto(calcElements, window, {cloneFunctions: true});
        } else {
            var script = document.createElement('script');
            script.textContent = 'let maxItems = ' + maxItems + '; ';
            script.textContent += 'document.querySelector(\'#primary > ytd-rich-grid-renderer\').calcElementsPerRow = ' + calcElements.toString();
            (document.head || document.documentElement).appendChild(script);
            script.remove();
        }
        cw.dispatchEvent(new Event('resize'));
    });
}

function addCustomCss() {
    let browse = window.chrome || browser;
    browse.storage.local.get(['css'], (res) => {
        let avatarHide = false;
        if (typeof res.css === 'undefined') {
            return;
        }
        if (typeof res.css.avatarHide !== 'undefined') {
            avatarHide = res.css.avatarHide;
        }
        var style = document.createElement('style');
        if (avatarHide) {
            style.textContent += '#avatar-link.ytd-rich-grid-video-renderer { display: none; }\n';
        }
        (document.head || document.documentElement).appendChild(style);
    });
}

(function () {
    addCustomCss();
    fixLayout();

    window.addEventListener("yt-navigate", function () {
        setTimeout(fixLayout, 1000);
    }, false);
})();
