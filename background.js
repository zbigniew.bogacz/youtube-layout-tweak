let browse = window.chrome || browser;
browse.runtime.onInstalled.addListener(async ({reason, temporary}) => {
    let url = '';
    switch (reason) {
        case "install":
            url = browse.runtime.getURL("installed.html");
            await browse.tabs.create({url});
            break;
        case "update":
            url = browse.runtime.getURL("updated.html");
            await browse.tabs.create({url});
            break;
    }
});
