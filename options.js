let browse = window.chrome || browser;

function saveOptions(e) {
    browse.storage.local.set({
        items: document.querySelector("#video-items").value,
        css: {
            avatarHide: document.querySelector("#css-avatarHide").checked,
        }
    });
    e.preventDefault();
}

function restoreOptions() {
    browse.storage.local.get(['items'], (res) => {
        document.querySelector("#video-items").value = res.items || 6;
    });
    browse.storage.local.get(['css'], (res) => {
        if (typeof res.css === 'undefined') {
            return;
        }
        document.querySelector("#css-avatarHide").checked = res.css.avatarHide;
    });
}

document.addEventListener('DOMContentLoaded', restoreOptions);
document.querySelector("body").addEventListener("change", saveOptions);
